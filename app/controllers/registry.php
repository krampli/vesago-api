<?php

# Global
Vesago::route( '/', function() { 
	return fetch( 'maintenance' );
});

# Update
Vesago::route( '/api/update/latest', 'UpdateController::latest' );