<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
	<title>VESAGO</title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500&display=swap" rel="stylesheet">
	<style type="text/css">
	body, html{
		padding: 25px 50px;
		font-family: 'Montserrat', sans-serif;
		background-color: #e5e5e5;
	}
	.window{
		background-color: #ffffff;
		padding: 25px 50px;
		margin: 0 auto 20px auto;
		box-shadow: 0 1px 0 rgb(0 0 0 / 4%), 0 4px 8px rgb(0 0 0 / 3%);
		transition: all 0.1s ease-out;
		max-width: 1000px;
	}
	.text-center{
		text-align: center;
	}
	.copyright,
	.copyright a{
		text-decoration: none;
		color: #29393f;
		font-size: 12px;
	}
	.copyright{
		margin-top: 50px;
	}
	</style>
	
</head>
<body>

	<div class="window text-center">
		
		<h1>Сервис временно недоступен</h1>
		<h3>Пожалуйста возвращайтесь позже</h3>
		
		<div class="copyright">
			© 2018 - 2021 <a href="https://krampli.com" target="_blank">KRAMPLI</a>
		</div>

	</div>

</body>
</html>