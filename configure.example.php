<?php 

/** Режим отладки */
define( 'DEBUG', false ); 
/** Отладка MySQL */
define( 'SQL_DEBUG', false );

/** Имя базы данных */
define( 'DB_NAME', '{{ DB_NAME }}' );
/** Имя пользователя MySQL */
define( 'DB_USER', '{{ DB_USER }}' );
/** Пароль к базе данных MySQL */
define( 'DB_PASS', '{{ DB_PASS }}' );
/** Имя сервера MySQL */
define( 'DB_HOST', '{{ DB_HOST }}' );

/** Кодировка MySQL */
define( 'DB_CHARSET', 'utf8' );

/** Режим MySQL */
// define( 'SQL_MODE', false );

/** Временная зона */
// define( 'TIMEZONE', '+06:00' );

/** Минификация вывода */
define( 'MINIFY_MODE', false );