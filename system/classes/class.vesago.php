<?php

class Vesago
{
	private static $i;
    private static $pool = [];
    
    /*
    * Возвращаем инстантинацию дочерних классов
    *
    */
    public function __get( $name ) 
    {   
        // Проверяем инстантинацию
        if( ! isset( self::$pool[ $name ] ) ):

            // Доступ к дочерним объектам
            if( ( $path = realpath( __DIR__ . "/class.$name.php" ) ) !== false ):

                if( require( $path ) ) return ( self::$pool[ $name ] = new $name() );

            endif;

        endif;

        return self::$pool[ $name ];
	}

    /*
    * Возвращаем текущию версию Vesago
    *
    */
    public static function v()
    {   
        return 0.005;
    }

    /*
    * Возвращаем инстантинацию класса Vesago
    *
    */
    public static function i()
    {   
        return ( ! isset( self::$i ) ? ( self::$i = new Vesago() ) : self::$i );
    }

    /*
    * Возвращаем инстантинацию моделей
    *
    */
    public static function m( $name )
    {   
        return Vesago::i()->model->$name;
    }

    /*
    *
    */
    public static function route( $url, $callback, $bind = null )
    {   
        return self::i()->router->add( $url, $callback, $bind );
    }

    /*
    *
    */
    public static function run()
    {   
        if( $route = self::i()->router->match() )
        {
            // Вызываем коллбэк
            if( is_callable( $route['callback'] ) ) {
                print( @ call_user_func( $route['callback'] ) );
            }   
        }
    }        
    
}