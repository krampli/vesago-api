<?php
/**
* 
* 
*/
class Setting extends Vesago
{
    private static $pool = [];

    public function __construct()
    {   
        // Устанавливаем настройки приложения
        foreach ( Vesago::i()->db->select( 'settings' ) as $i => $setting ) 
        {
            self::$pool[ $setting['name'] ] = $setting['value'];
        }
    }

    /*
    * Возвращает значение настройки приложения
    *
    */
    public function __get( $name ) 
    {       
        return ( isset( self::$pool[ $name ] ) ) ? self::$pool[ $name ] : null; 
    }

    /*
    *
    */
    public function __set( $name, $value ) 
    {
        // 
        if( is_string( $value ) || is_numeric( $value ) )
        {
            // Если это новый элемент
            if( ! array_key_exists( $name , self::$pool ) )
            {       
                Vesago::i()->db->insert( 'settings' , [
                    'name'  => $name,
                    'value' => $value,
                ]);
            }
            else
            {
                // Если есть изменения обновляем
                if( self::$pool[ $name ] != $value ) 
                {
                    Vesago::i()->db->update( 'settings' , [
                        'data' => [
                            'value' => $value
                        ],
                        'where' => [ [ 'name' , '=' , $name  ] ]
                    ]);
                }
            }

            // Меняем текущие
            self::$pool[ $name ] = $value;
        }

        return $value;
    }

    /*
    *
    */
    public function __isset( $name ) 
    {
        return isset( self::$pool[ $name ] );
    }

    /*
    *
    */
    public function __unset( $name ) 
    {
        Vesago::i()->db->delete( 'settings' , [
            'where' => [ [ 'name' , '=' , $name  ] ]
        ]);
    }
}