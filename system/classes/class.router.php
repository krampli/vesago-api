<?php

use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class Router extends Vesago
{

    private static $routes = null;

    /*
    *
    *
    */
    function __construct()
    {
        // Истантинация коллекции роутов
        if( ! isset( self::$routes ) ) {
            self::$routes = new RouteCollection();
        }

        return self::$routes;
    }
	
    /*
    *
    *
    */
    function add( $url, $callback, $bind = null )
    {
        return self::$routes->add( 
            ( empty($bind) ) ? uniqid() : $bind , new Route( $url, [ 'callback' => $callback ] )
        );
    }

    /*
    *
    *
    */
    function urlg( $alias, $parms = [] )
    {
        // Init RequestContext object
        $context = new RequestContext();
        $context->fromRequest( Request::createFromGlobals() );

        $generator = new UrlGenerator( self::$routes, $context );
        return $generator->generate( $alias );
    }

    /*
    *
    *
    */
    function match()
    {
        try
        {
            // Init RequestContext object
            $context = new RequestContext();
            $context->fromRequest( Request::createFromGlobals() );

            // Init UrlMatcher object
            $matcher = new UrlMatcher( self::$routes, $context );

            // Find the current route
            return $matcher->match( $context->getPathInfo() );

        }
        catch ( ResourceNotFoundException $e )
        {
          echo $e->getMessage();
        }
        catch ( Exception $e )
        {
          echo $e->getMessage();
        }
    }
    
}