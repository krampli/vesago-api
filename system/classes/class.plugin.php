<?php
/**
* 
* 
*/
class Plugin extends Vesago
{
	private $actions = [];
	private $filters = [];

	public function add_action( $event, $callback, $priority )
	{
		$this->actions[ $event ][] = [
			'callback' => $callback,
			'priority' => $priority
		];
	}

	public function add_filter( $event, $callback, $priority )
	{
		$this->filters[ $event ][] = [
			'callback' => $callback,
			'priority' => $priority
		];
	}

	/*
	* Вызов обработчиков при осуществлении события
	*
	*/
	public function event( $name )
	{
		// Если нет событий
		if( ! isset( $this->actions[ $name ]) )
			return false;

		$tail = []; 

		// Сортировка очереди
		foreach ( $this->actions[ $name ] as $i => $item ) 
		{
			$tail[ $item["priority"] ][] = $item['callback'];
		}

		ksort($tail);

		// Вызов плагинов
		foreach ( $tail as $priority => $list )
		foreach ( $list as $i => $callback ) 
		{
			call_user_func( $callback );
		}
	}

	/*
	* Обработка переменной (фильтрация)
	*
	*/
	public function filter( $name, $stream )
	{
		// Если нет фильтров
		if( isset( $this->filters[ $name ] ) )
		{
			$tail = []; 

			// Сортировка очереди
			foreach ( $this->filters[ $name ] as $i => $item ) 
			{
				$tail[ $item["priority"] ][] = $item['callback'];
			}

			ksort($tail);

			// Вызов плагинов
			foreach ( $tail as $priority => $list )
			foreach ( $list as $i => $callback ) 
			{
				call_user_func( $callback , $stream );
			}
		}

		return $stream;
	}
}