<?php
/**
* 
* 
*/
class Request extends Vesago
{	

    /**
    *
    *
    */
    public function protocol()
    {
        return strtolower( substr( 
            $_SERVER["SERVER_PROTOCOL"], 0, strpos( $_SERVER["SERVER_PROTOCOL"] , '/' ) 
        ) ) . '://';
    }

	/**
    * Определение request-метода обращения к странице (GET, POST)
    *
    */
    public function method( $method = null ) 
    {
        if( !empty( $method ) ) 
        {
            return strtolower( $_SERVER['REQUEST_METHOD'] ) == strtolower( $method );
        }

        return $_SERVER['REQUEST_METHOD'];
    }

    /*
    * Возвращаем GET переменную
    *
    */
	public function get( $name = null, $type = null, $mask = null )
	{
		$value = null;
        
        if( ! empty( $name ) && isset( $_GET[$name] ) ) 
        {
            $value = $_GET[$name];
        }
        
        return $this->filter( $value, $type );
	}

	/*
    * Возвращаем POST переменную
    *
    */
	public function post( $name = null, $type = null, $mask = null )
	{
		$value = null;
        
        if( ! empty( $name ) && isset( $_POST[$name] ) ) 
        {
            $value = $_POST[$name];
        }

        return $this->filter( $value, $type, $mask );
	}

    /*
    *
    */
    public function filter( $value, $type = null, $mask = null )
    {
        if( $type == 'str' || $type == 'string' ) 
        {
            return strval( preg_replace('/[^\p{L}\p{Nd}\d\s_\-\.\%\s]/ui', '', $value ) );
        }

        if( $type == 'int' ) 
        {
            return intval( $value );
        }

        if( $type == 'float' ) 
        {
            return floatval( $value );
        }

        if( $type == 'bool' ) 
        {
            return !empty( $value );
        }

        if( $type == 'phone' )
        {
            return preg_replace('/\D/', '', $value );
        }

        if( $type == 'date' )
        {
            return (empty($value)) ? null : date( $mask , strtotime( $value ) );
        }
        
        return $value;
    }

}