<?php
/**
* 
* 
*/
class Session extends Vesago
{
    private $session = [];

    public function __construct()
    {
        if( session_status() == PHP_SESSION_NONE )
        {
            exit("You forgot to call session_start();");
        }
    }

    // Авторизация пользователя
    public function logged()
    {
        return isset( $this->userdata['logged'] ) ? boolval( $this->userdata['logged'] ) : false;
    }

    /*
    *
    */
    public function __get( $var ) 
    {       
        return ( isset( $_SESSION[ $var ] ) ) ? $_SESSION[ $var ] : null; 
    }

    /*
    *
    */
    public function __set( $var, $val ) 
    {
        if( is_string( $val ) || is_array( $val ) || is_numeric( $val ) )
        {
            $_SESSION[ $var ] = $val;
        }

        return true;
    }

    /*
    *
    */
    public function __isset( $var ) 
    {
        return isset( $_SESSION[ $var ] );
    }

    /*
    *
    */
    public function __unset( $var ) 
    {
        unset( $_SESSION[ $var ] );
    }

}