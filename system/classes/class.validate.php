<?php
/**
* 
* 
*/
class Validate extends Vesago
{	
    private $blacklist = ["<iframe", "</iframe", "<script", "</script"];

    /*
    *
    */
    public function is_email( $email, $required = false ) 
    {   
        if( ! $this->is_safe( $email ) ) 
            return false;
        
        if( empty( $email ) ) 
            return ( $required ) ? false : true;
    
        if( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) 
            return true;

        return false;
    }

    /*
    *
    */
    public function is_phone( $phone, $required = false) {
        
        if( $this->is_safe( $phone ) ) 
            return false;
        
        if( empty( $phone ) ) 
            return ( $required ) ? false : true;

        if( preg_match("~([^0-9 _\+\-\(\)]+)~", $phone) ) 
            return false;

        if( strlen(preg_replace("~[^\d]~", "", $phone)) < 7) 
            return false;

        return true;
    }

    /*
    *
    */
    public function is_safe( $string, $required = false ) 
    {
        if( ! empty( $string ) ) 
        {
            foreach ( $this->blacklist as $signature ) 
            {
                if ( strpos( $string, $signature ) !== false ) {
                    return false;
                }
            }
        } 
        elseif ( $required )
        {
            return false;
        }

        return true;
    }
}