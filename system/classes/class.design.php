<?php
/**
* 
* 
*/
class Design extends Vesago
{	
	// Объявленные переменные
	public static $vars = [];

	/**
	* Генерация отображения
	*
	*/
	public static function fetch( $alias , $vars = [] )
	{	
		// Объединение массивов переменных
		ob_start(); extract( self::$vars = array_merge( $vars, self::$vars ) );

		// Попытка подключить шаблон
		if( ( $path = self::realias( $alias ) ) !== false ) : include $path; else: ?>
			
			<div style="background-color: #ec0928; color: #fff; padding: 15px 25px; margin: 10px;">
				Template no found ( <?=$alias ?> )
			</div>

		<?php endif;

		// Возвращаем буфер
		return self::minify( ob_get_clean() );
	}

	/**
	* Формирует абослютный путь к шаблону по алиасу
	*
	*/
	public static function realias( $alias )
	{	
		// Удаляем лишние символы
		$path = trim( strtr( $alias, [
			'.' => '/', '.php' => '', ':' => ''
		]));

		// Шаблоны приложения
		if(( $tpl = realpath( ABS_PATH . "/app/templates/$path.php" )) !== false ) return $tpl;
		else
		{
			// Системные шаблоны 
			return realpath( SYS_PATH . "/app/templates/$path.php" );
		}
	}

	/*
	* Минификация вывода
	*
	*/
	public static function minify( $stream )
	{	
		if( MINIFY_MODE )
		{
			// Возвращаем буфер
			return strtr( $stream , [

				"\r" => '', "\n" => '', "\t" => ''

			]);
		}
		
		return $stream;
	}

	/*
	* Работа с переменными шаблона
	*
	*/
	public static function assign( $name, $value = null )
	{
		// Установка значения
		if( isset( $value ) ) 
		{
			if( is_array( $name ) )
			{	
				// Создаем первичный массив
				$i = []; $i[ ($bind = implode( '.' , $name )) ] = $value;

				// Передаем данные в пул
				self::$vars = array_merge( self::$vars, $i );

				// Возвращем данные
				return self::$vars[ $bind ];
			}

			return self::$vars[ $name ] = $value;
		}

		return self::$vars[ $name ];
	}

	/*
	*
	*/
	public static function var( $name )
	{
		// Возвращаем массив
		if( is_array( $name ) )
		{	
			if( array_key_exists( $name[1] , self::$vars[ $name[0] ] ) )
				return self::$vars[ $name[0] ][ $name[1] ];

			return [];
		}

		return self::$vars[ $name ];
	}

}