<?php

class Model extends Vesago
{
	private static $i;
    private static $pool = [];
    
    /*
    * Возвращаем инстантинацию дочерних классов
    *
    */
    public function __get( $name ) 
    {   
        // Проверяем инстантинацию
        if( ! isset( self::$pool[ $name ] ) ):

            // Доступ к моделям приложения
            if( ( $path = realpath( ABS_PATH . "/app/models/class.$name.php" ) ) !== false )
            {
                if( require( $path ) ) return ( self::$pool[ $name ] = new $name() );
            }
            // Доступ к системым моделям
            elseif ( ( $path = realpath( SYS_PATH . "/models/class.$name.php" ) ) !== false )
            {
                if( require( $path ) ) return ( self::$pool[ $name ] = new $name() );
            } 

        endif;

        return self::$pool[ $name ];
	}    
    
}