<?php
/**
* 
* 
*/
class Db extends Vesago
{   
    private $i, $r;

    /**
    * Установка соединения с БД
    */
    public function __construct()
    {
        $this->connect();
    }

    /**
    * Разрыв соединения с БД
    */
    public function __destruct()
    {
        $this->disconnect();
    }

    /**
    * Подключение к базе данных
    */
    public function connect()
    {
        // При повторном вызове возвращаем инстантинацию
        if( ! empty( $this->i ) ) return $this->i;
        
        // Иначе устанавливаем соединение
        else
        {   
            @ $this->i = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
        }

        // Выводим сообщение, в случае ошибки
        if( $this->i->connect_error )
        {
            return $this->i->connect_error;
        }

        // Или настраиваем соединение
        else
        {
            /** 
            * Дополнительная настройка соединения 
            *
            */

            if( defined('DB_CHARSET') )
                $this->i->query("SET NAMES SQL_MODE = '". DB_CHARSET ."'");

            if( defined('SQL_MODE') )
                $this->i->query("SET SESSION SQL_MODE = '". SQL_MODE ."'");

            if( defined('TIMEZONE') )
                $this->i->query("SET time_zone = '". TIMEZONE ."'");

            /* */
            $this->i->query("SET NAMES utf8");
            $this->i->query("SET CHARACTER SET utf8");
        }
        
        return $this->i;
    }

    /**
    * Разрыв соединения с базой данных
    *
    */
    public function disconnect()
    {
        return @ $this->i->close();
    }

    /**
    * Выполняем запрос
    *
    */
    public function exec( $SQL )
    {   
        // Режим отладки SQK запросов
        if( defined('SQL_DEBUG') && SQL_DEBUG ) 
        {
            file_put_contents( ABS_PATH . '/logs/mysqli.log', "$SQL \n", FILE_APPEND );
        }

        return $this->r = $this->i->query( $SQL );   
    }

    /**
    * Возвращает данные в виде массива
    *
    */
    public function results()
    {
        // Ссылка на результат отсутствует
        if( ! $this->r ) exit( trigger_error( $this->i->error , E_USER_WARNING ) );

        // Формируем данные из ответа
        if( is_object( $this->r ) && $this->r->num_rows != 0 )
        {
            $results = []; 

            while( $row = $this->r->fetch_object() )
            {
                array_push( $results, (array) $row );
            }

            return $results;
        }

        return [];
    }

    /**
    * Выборка данных из таблицы
    *
    */
    public function select( $table, $request = [] )
    {
        // Значения по-умолчанию
        extract([
            'only'     => false, 
            'where'    => false,
            'where_as' => false,
            'limit'    => false, 
            'offset'   => false, 
            'order'    => false, 
            'order_by' => false
        ]);

        // Переданные значения
        extract( $request );

        // Формируем запрос
        $this->exec("SELECT {$this->only($only)} FROM `$table` {$this->where($where, $where_as)} {$this->order($order_by, $order)} {$this->limit($offset, $limit)}");
        
        // Возвращаем результаты
        return $this->results();
    }

    /**
    * Формирует выборку колонок
    *
    */
    private function only( $value )
    {
        if( $value === false ) return '*';

        if( is_array( $value ) )
        {
            foreach ( $value as $i => $v ) 
                $value[$i] = "`$v`"; 
        
            return join(",", $value);
        }

        return "`$value`";
    }

    /**
    * Формирует условие выборки
    *
    */
    private function where( $value, $where_as = false )
    {
        $where_as = ( $where_as === false ) ? ' AND ' : $where_as;

        if( is_array( $value ) )
        {
            foreach ( $value as $i => $line ) 
            {
                if( is_array( $line[2] ) )
                {
                    $value[$i] = " `{$line[0]}` {$line[1]} (".implode( ',', $line[2] ).")";
                }
                else
                {
                    $value[$i] = " `{$line[0]}` {$line[1]} '{$line[2]}'";
                }
            }
        
            return " WHERE " . join( $where_as, $value );
        }
    }

    /**
    * Формирует сортировку выборки
    *
    */
    private function order( $order_by, $order )
    {
        if( $order_by !== false && $order !== false )
        {
            return " ORDER BY `$order_by` $order";
        }
    }

    /**
    * Формирует лимит выборки
    *
    */
    private function limit( $offset, $limit )
    {
        if( $limit !== false ) 
            return " LIMIT ".intval($offset).", ".intval($limit);
    }

    /**
    * Добавляет запись в таблицу
    *
    */
    public function insert( $table, $request )
    {
        // Это массив?
        if( ! is_array( $request ) )
            return false;

        // Формируем запрос
        if( count( $request ) == 0 ) return false;
        else
        {
            $cols = []; $vals = []; foreach ( $request as $col => $val ) 
            {
                $cols[] = "`$col`"; $vals[] = ( $val === '' ) ? 'NULL' : "'$val'";
            }
        }

        // Выполняем запрос к базе
        $this->exec(
            "INSERT INTO `$table` (".join(",", $cols).") VALUES (".join(",", $vals).")"
        );

        // Возвращаем ID элемента
        return $this->i->insert_id;
    }

    /**
    * Обновляем записи в таблице
    *
    */
    public function update( $table, $request )
    {
        // Значения по-умолчанию
        extract([
            'data'     => [], 
            'where'    => false,
            'where_as' => false
        ]);

        // Переданные значения
        extract( $request );

        // Это массив?
        if( ! is_array( $data ) )
            return false;

        // Формируем запрос
        if( count( $data ) == 0 ) return false;
        else
        {
            $lines = []; 

            foreach ( $data as $col => $value )
            {
                $value = ( empty($value) ) ? 'NULL' : "'$value'"; $lines[] = "`$col` = $value";
            }
        }

        // Формируем запрос
        $this->exec("UPDATE `$table` SET ".join(',', $lines)." {$this->where($where, $where_as)}");
    }

    /**
    * Удаляет запись в таблице
    *
    */
    public function delete( $table, $request )
    {
        // Значения по-умолчанию
        extract([
            'where'    => false,
            'where_as' => false
        ]);

        // Переданные значения
        extract( $request );

        // Выполняем запрос к базе
        $this->exec("DELETE FROM `$table` {$this->where($where, $where_as)}");

        // Возвращаем результаты
        return true;
    }

    /**
    * Буливая смена значения в колонке
    *
    */
    public function toggle( $table, $request )
    {
        // Значения по-умолчанию
        extract([
            'data'     => [], 
            'where'    => false,
            'where_as' => false
        ]);

        // Переданные значения
        extract( $request );

        // Это массив?
        if( ! is_array( $data ) )
            return false;

        // Формируем запрос
        if( count( $data ) == 0 ) return false;
        else
        {
            $lines = []; foreach ( $data as $col ) $lines[] = "`$col` = '1' - `$col`";
        }

        // Формируем запрос
        $this->exec("UPDATE `$table` SET ".join(',', $lines)." {$this->where($where, $where_as)}");

        // Возвращаем результаты
        return $this->results();
    }

}