<?php

# Global
Vesago::route( '/', 'MainController::main', 'mainpage' );

# Manager
Vesago::route( '/manager/dashboard', 'ManagerController::dashboard', 'manager.dashboard' );
Vesago::route( '/manager/auth', 'ManagerController::auth', 'manager.auth' );