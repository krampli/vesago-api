<?php

/**
* 
* 
*/
class User extends Vesago
{
    /*
    * Возвращает id текущего пользователя
    *
    */
    public function id()
    {
        if( Vesago::i()->session->logged() )
        {
            return intval( Vesago::i()->session->userdata['id'] );
        }
        
        return 0;
    }

    /*
    * Установка сессии
    *
    */
    public static function assign( $id, $selector = 'id' )
    {   
        //
        $userdata = Vesago::i()->db->select( 'users', [
            'only'  => ['id','activated','login','email','activity','created'],
            'where' => [
                [ $selector , '=' , $id ]
            ],
            'limit' => 1
        ]);

        //
        if( count( $userdata ) > 0 )
        {
            $userdata = (array) reset( $userdata );

            // Обновляем активность пользователя
            self::activity( $userdata['id'] );

            // Получаем список групп пользователя
            $userdata['groups'] = []; foreach ( Vesago::i()->groups->all( $userdata['id'] ) as $i => $item ) {
                $userdata['groups'][] = $item['type'];
            }

            // Обявление сессии
            Vesago::i()->session->userdata = array_merge([ 
                
                'logged' => true 

            ], $userdata );

            return true;
        }
        else
        {
            Vesago::i()->session->userdata = [ 'logged' => false ];
        }

        return false;
    }

    /*
    * Аунтификация пользователя
    *
    */
    public static function auth( $selector, $password, $type = 'login' )
    {
        // Получаем пользователя
        $userdata = Vesago::i()->db->select( 'users', [
            'only' => ['id','hash'],
            'where' => [
                [ $type ,'=', $selector ],
                ['activated','=','1']
            ],
            'limit' => 1
        ]);

        // Если пользователь не существует
        if( count( $userdata ) > 0 )
        {
            $userdata = reset( $userdata );

            if( password_verify( $password, $userdata['hash'] ) )
                return self::assign( $userdata['id'] );
        }

        return false;
    }

    /*
    * Обновления активности пользователя
    *
    */
    public static function activity( $id ) {

        // Получаем данные об аккаунте
        Vesago::i()->db->update( 'users' , [
            'data'  => [ 'activity' => date("Y-m-d H:i:s") ],
            'where' => [ 
                [ 'id' , '=' , $id  ] 
            ]
        ]);
    }

    /*
    *
    */
    public static function find( $selector , $type = 'email', $limit = 1 )
    {
        // Получаем пользователя
        $userdata = Vesago::i()->db->select( 'users', [
            'where' => [
                [ $type ,'=', $selector],
                ['activated','=','1']
            ],
            'limit' => $limit
        ]);

        // Если пользователь не существует
        if( count( $userdata ) > 0 )
        {
            return reset( $userdata );
        }

        return [];
    }


    /*
    *
    */
    public static function add( $userdata = [] )
    {
        // Шаблон данных
        extract([
            'activated' => 1,
            'created'   => date("Y-m-d H:i:s")
        ]);

        // Входящие данные
        extract( $userdata );

        // Проверяем данные
        if( isset( $login, $email, $phone, $password ) )
        {
            if( filter_var( $email, FILTER_VALIDATE_EMAIL ) && ! self::exists( $email, 'email' ) )
            {
                // Хэширование
                $hash = password_hash( $password, PASSWORD_BCRYPT );

                // Добавление записи и возвращаем id аккаунта
                return Vesago::i()->db->insert( 'users', [
                    'activated' => $activated,
                    'login'     => $login,
                    'email'     => $email,
                    'phone'     => $phone,
                    'hash'      => $hash,
                    'created'   => $created
                ]);
            }
        }

        return 0;
    }

    /*
    * Проверям существование аккаунта 
    *
    */
    public static function exists( $selector , $type = 'id' )
    {
        $result = Vesago::i()->db->select( 'users', [
            'only'  => ['id'],
            'where' => [
                [ $type , '=' , $selector ]
            ],
            'limit' => 1
        ]);

        return boolval( count($result) );
    }
    
    /*
    * Возвращает запись об аккаунте
    *
    */
    public static function single( $selector , $type = 'id' )
    {
        $result = Vesago::i()->db->select( 'users', [
            'where' => [
                [ $type , '=' , $selector ]
            ],
            'limit' => 1
        ]);

        if( count($result) > 0 )
            return reset($result);
           
        return [];
    }

    /*
    * Смена состояния аккаунта
    *
    */
    public static function toggle( $id )
    {
        Vesago::i()->db->toggle( 'users', 'activated', ['id','=', intval($id)] );
    }

    /*
    * Фильтрация данных
    *
    */
    public function filter( $data )
    {
        // Формирование слага элемента
        if( !empty( $data['slug'] ) ) 
        {
            $data['slug'] = slugtf( $data['slug'] );
        }
        elseif( !empty( $data['title'] ) )
        {
            $data['slug'] = slugtf( $data['title'] );
        }

        return $data;
    }

    /*
    *
    *
    */
    public function all( $where = [], $option = false )
    {   
        $filter = [];

        // Доп. параметры
        if( $option !== false )
        {
            if( is_numeric( $option ) )
            {
                $filter['limit'] = intval( $option );
            }

            if( is_array( $option ) )
            {
                $filter = array_merge( $filter, $option );
            }
        }

        // Формируем фильтр
        $filter['where'] = $where;

        // Выполняем условия фильтра
        $users = Vesago::i()->db->select( 'users', $filter );   

        // Если элменты найдены
        if( count( $users ) > 0 )  
        {
            return $users;
        }

        return [];
    }

    /*
    * Осуществляет поиск элемента по типу и одиночному условию
    *
    * @format: ('page', ['active', '=', 0], 10);
    *
    */
    public function findBy( $where = [] )
    {
        //  
        if( is_numeric( $where ) )
        {
            $where = [ [ 'id', '=', intval( $where ) ] ];
        }

        //
        $users = Vesago::i()->db->select( 'users', [
            'where' => $where, 'limit' => 1
        ]);

        // 
        if( count( $users ) > 0 )  
        {
            return reset( $users );
        }

        return false;
    }

    /*
    * Изменения состояния элемента на противоположное
    *
    */
    public function update( $where = [] , $data = [] )
    {   
        // Фильтрация данных
        $data = $this->filter( $data );

        // 
        if( is_numeric( $where ) )
        {
            $where = [ [ 'id', '=', intval( $where ) ] ];
        }

        // Выполняем обновление
        Vesago::i()->db->update( 'users' , [
            
            'data'  => array_merge( 
                $data, [ 'updated' => date("Y-m-d H:i:s") ] 
            ), 
            
            'where' => $where

        ]); 

        return true;
    }

    /*
    * Удаление элемента
    *
    */
    public function delete( $where = [] )
    {   
        // 
        if( is_numeric( $where ) )
        {
            $where = [ [ 'id', '=', intval( $where ) ] ];
        }

        // Выполняем обновление
        Vesago::i()->db->delete( 'users' , [

            'where' => $where

        ]);

        return true;
    }
}