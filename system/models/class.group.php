<?php
/**
* 
* 
*/
class Group extends Vesago
{
    /*
    * Установка сессии
    *
    */
    public static function all( $user_id )
    {   
        //
        $groups = Vesago::i()->db->select( 'users__groups', [
            'where' => [
                [ 'user_id' , '=' , $user_id ]
            ],
        ]);

        //
        if( count( $groups ) > 0 )
        {
            return $groups;
        }

        return [];
    }

}