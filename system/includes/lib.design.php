<?php

/*
*
*/
function fetch( $alias, $vars = [] )
{
	return Vesago::i()->design->fetch( $alias , $vars );
}

/*
*
*/
function assign( $name, $value = NULL )
{
	return Vesago::i()->design->assign( $name, $value );
}