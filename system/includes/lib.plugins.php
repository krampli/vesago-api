<?php

/*
* Регистрация события
*
*/
function event( $name )
{
	return Vesago::i()->plugin->event( $name );
}

/*
* Выполнение события
*
*/
function filter( $name, $stream )
{
	return Vesago::i()->plugin->filter( $name, $stream );
}

/*
* Регистрация плагина на событие
*
*/
function add_action( $event, $callback, $priority = 0 )
{	
	// Добавляем вызов плагина
	return Vesago::i()->plugin->add_action( $event, $callback, $priority );
}

/*
* Регистрация фильтра на событие
*
*/
function add_filter( $event, $callback, $priority = 0 )
{
	// Добавляем вызов плагина
	return Vesago::i()->plugin->add_filter( $event, $callback, $priority );
}
