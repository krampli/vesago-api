<?php

/*
* Функция для мультиязычности
*
*/
function _e( $string )
{   
    // Реализация переводов 
    if(( $language = session('language') )) 
    {
        return Vesago::i()->language->echo( $string, $language );
    }

    return $string;
}

/*
* Рекурсивная функция glob()
*
*/
function rglob( $pattern, $flags = 0 ) 
{
    $files = glob( $pattern, $flags ); 

    foreach ( glob( dirname( $pattern ) .'/*' , GLOB_ONLYDIR | GLOB_NOSORT ) as $dir ) 
    {
        $files = array_merge( $files, rglob( $dir . '/'.basename( $pattern ), $flags ) );
    }

    return $files;
}

/*
* Взаимодействие с настройками
*
*/
function setting( $name , $value = null )
{   
    if( $value !== null ) 
    {
        return Vesago::i()->setting->$name = $value; 
    }
    else
    {
        return Vesago::i()->setting->$name;
    }
}

/*
* Взаимодействие с сессией
*
*/
function session( $var , $val = false )
{   
    if( $val !== false ) 
    {
        return Vesago::i()->session->$var = $val; 
    }
    else
    {
        return Vesago::i()->session->$var;
    }
}

/*
* Возвращает пользовательские данные
*
*/
function userdata( $name = null )
{
    if( ! empty( $userdata = Vesago::i()->session->userdata ) )
    {
        if( ! empty( $name ) )
        {
            return ( array_key_exists( $name , $userdata ) ? $userdata[ $name ] : null );
        }

        return $userdata;
    }
    else
    {
        if( ! empty( $name ) )
        {
            return null;
        }

        return [];
    }
}

/*
* Проверят направлен ли запрос в консоль
*
*/
function is_console()
{
	return boolval( stripos( $_SERVER["REQUEST_URI"], '/console' ) === 0 );
}

/*
*
*
*/
function logged()
{
    return Vesago::i()->session->logged();
}

/*
* Проверят входит ли пользователь в группу
*
*/
function usertype( $type )
{
    if( is_array( userdata('groups') ) ) {
        return in_array( $type, userdata('groups') );
    }

    return false;
}

/*
* Формирование URL по алиасу контроллера
*
*/
function urlg( $alias, $parms = [] )
{	
	return Vesago::i()->router->urlg( $alias, $parms );
}

/*
* Создание редиректа по алиасу контроллера
*
*/
function redirect( $alias, $parms = [] )
{
	exit( header( 'Location: ' . urlg( $alias, $parms ) ) );
}

/*
* Трансформирует строку в слаг
*
*/
function slugtf( $string )
{
	// Декодируем строку и переводим нижний регистр
    $string = mb_strtolower( urldecode( $string ) );
    
    // Удаляем все символы кроме разрешенных
    $string = preg_replace( '/[^а-я0-9]-_/ui', '', $string );

    // Словарь
    $dictionary = array(
        'а' => 'a',  'б' => 'b',  'в' => 'v', 
        'г' => 'g',  'д' => 'd',  'е' => 'e', 
        'ё' => 'e',  'ж' => 'zh', 'з' => 'z', 
        'и' => 'i',  'й' => 'i',  'к' => 'k', 
        'л' => 'l',  'м' => 'm',  'н' => 'n', 
        'о' => 'o',  'п' => 'p',  'р' => 'r', 
        'с' => 's',  'т' => 't',  'у' => 'u', 
        'ф' => 'f',  'х' => 'kh', 'ц' => 'ts',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 
        'ъ' => 'ie', 'ы' => 'y',  'ь' => '', 
        'э' => 'e',  'ю' => 'iu', 'я' => 'ia',
        ' ' => '-',  '!' => '',   '#' => '', 
        '?' => ''
    );

    return strtr( $string, $dictionary );
}