<?php 

/** Определяем системную директорию  **/
if( ! defined( 'SYS_PATH' ) ) : define( 'SYS_PATH' , __DIR__ );

	/** Загружаем библиотеки приложения **/
	foreach( glob( SYS_PATH . '/includes/*.php' ) as $i => $path )
		require_once $path;

	$paths = [
		rglob( ABS_PATH . '/app/controllers/*.php' ),
		rglob( SYS_PATH . '/app/controllers/*.php' ),
	];

	/** Загружаем контроллеры и роуты **/
	foreach( $paths as $list ) foreach( $list as $i => $path )
	{
		require_once realpath( $path );
	}
	
endif;