<?php session_start();

/** Определяем корневную директорию  **/
if( ! defined( 'ABS_PATH' ) ) : define( 'ABS_PATH' , __DIR__ );

	/** Загружаем зависимости приложения **/
	require_once ABS_PATH . '/vendor/autoload.php';
	
	/** Загружаем родительский класс **/
	require_once ABS_PATH . '/system/classes/class.vesago.php';

	/** Определяем публичную директорию **/
	if( ! defined( 'WEB_PATH' ) ) {
		define( 'WEB_PATH' , realpath( ABS_PATH . '/public/' ) );
	}

	/** Загружаем файл конфигурации **/
	if(( $configure = realpath( ABS_PATH . '/configure.php' )) !== false ): require $configure;

		/** Файл загружен. Загрузка приложения **/
		require_once ABS_PATH . '/system/runtime.php';

	else:

		/** Файл отсутствует. Конфигурирование приложения **/
		require_once ABS_PATH . '/system/deploy.php';
		
	endif;

endif;